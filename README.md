# Gateway
### Simple netflix zuul implementation
This is a sample zuul gateway.

There are three simple features added to this example

- Routing (to sample project which contains two apis `/order` and `/checkout`)
- Pre-filtering (filter every request to `/sample/checkout` which must have header `X-Signature` with value `HELLOWORLD`.
Otherwise the requests will be dropped)
- RateLimit (Currently it only applies with `/sample/checkout`)

### How to run 
#### Pre-requisits

- Check-out and run `sample` project. Check out [here](https://gitlab.com/pchaivong/sample)

1. Clone this repository `$ git clone https://gitlab.com/pchaivong/gateway.git`
2. Build and run `./mvnw spring-boot:run`


### Configurations

- Routing, `zuul.routes.sample.urls`. The default value will be directed to `sample` service
By default `sample` service is serving at port 8091. In case you've changed the `sample` serving port,
please ensure that you correct this configuration
- Port, `server.port` The default is `8090`.
- Rate Limit, `gateway.rateLimit.milliseconds` (This is the windows size) and
`gateway.rateLimit.requests` (this is a maximum number of request per windows size)



### Testing

`curl http://localhost:8090/sample/order` this request should not be filtered in any circumstances

`curl http://localhost:8090/sample/checkout` this request will be filtered. with out `X-Signature` header
the request will be dropped

`curl -H "X-Signature: HELLOWORLD" http://localhost:8090/sample/checkout` this is a valid request and
it will be valid when do the pre-filtering