package com.example.gateway.filter.pre;

import com.example.gateway.RateLimitBucket;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import java.util.Map;

public class SimpleFilter extends ZuulFilter{

    private static Logger log = LoggerFactory.getLogger(SimpleFilter.class);

    @Autowired
    private RateLimitBucket bucket;

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String url = ctx.getRequest().getRequestURI();
        log.info("Request to : " + url);
        if (url.contains("/checkout")){
            log.info("Request to /checkout is required to be filtered");
            return true;
        }
        else {
            log.info("Request to other path is not required filter");
            return false;
        }
    }

    @Override
    public Object run() {

        RequestContext ctx = RequestContext.getCurrentContext();
        String signature = ctx.getRequest().getHeader("X-Signature");

        if (!bucket.inc()){
            log.info("Request limit exceeded, reject request");
            ctx.setResponseStatusCode(429);
            ctx.setResponseBody("Rate limit exceeded");
            ctx.setSendZuulResponse(false);
            return null;
        }

        if (signature != null){
            if (signature.contains("HELLOWORLD")){
                log.info("Signature is correct, Accept request");
                return null;
            }
        }

        log.info("Signature is invalid, Reject request");
        ctx.setResponseStatusCode(401);
        ctx.setResponseBody("Unauthorized");
        ctx.setSendZuulResponse(false);
        return null;

    }

}
