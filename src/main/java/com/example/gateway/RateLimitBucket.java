package com.example.gateway;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class RateLimitBucket {

    private int currentRate;
    private int limitRate;

    private Logger log = LoggerFactory.getLogger(RateLimitBucket.class);


    public RateLimitBucket(int limitRate) {
        this.limitRate = limitRate;
        this.currentRate = 0;
    }

    public void reset(){
        this.currentRate = 0;
        log.info("Reset bucket");
    }

    public boolean inc(){
        if (this.limitRate == this.currentRate) return false;
        else {
            this.currentRate++;
            log.info("Current limit rate: " + currentRate);
            return true;
        }
    }
}
