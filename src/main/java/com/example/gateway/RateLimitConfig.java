package com.example.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Configuration
public class RateLimitConfig {

    private Logger logger = LoggerFactory.getLogger(RateLimitConfig.class);

    @Value("${gateway.rateLimit.requests}")
    int requests;

    @Value("${gateway.rateLimit.milliseconds}")
    int windowSize;

    @Bean
    public RateLimitBucket bucket(){
        logger.info("Create rate limit bucket");
        logger.info("Maximum rate: " + requests + " requests per " + windowSize + " milliseconds");
        return new RateLimitBucket(requests);
    }
}
