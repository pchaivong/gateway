package com.example.gateway;

import com.example.gateway.filter.pre.SimpleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@EnableZuulProxy
@EnableScheduling
@SpringBootApplication
public class GatewayApplication {

    private Logger logger = LoggerFactory.getLogger(GatewayApplication.class);

    @Autowired
    private RateLimitBucket rateLimitBucket;

    @Autowired
    private RateLimitConfig rateLimitConfig;

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Bean
    public TaskScheduler taskScheduler() {
	    final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
	    scheduler.setPoolSize(1);
	    return scheduler;
    }


    @Scheduled(fixedRateString = "${gateway.rateLimit.milliseconds}" )
    public void resetBucket() throws InterruptedException {
        rateLimitBucket.reset();
    }


	@Bean
    public SimpleFilter simpleFilter(){
	    return new SimpleFilter();
    }
}
